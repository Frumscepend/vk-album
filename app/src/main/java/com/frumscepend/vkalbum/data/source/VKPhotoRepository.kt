package com.frumscepend.vkalbum.data.source

import android.graphics.Bitmap
import com.frumscepend.vkalbum.data.Response
import com.frumscepend.vkalbum.data.source.cache.VKPhotoCacheDataSource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class VKPhotoRepository (
        private val vkPhotoCacheDataSource: VKPhotoCacheDataSource,
        private val vkPhotoRemoteDataSource: VKPhotoDataSource
): VKPhotoDataSource{

    override fun getPhotos(): Observable<Response> {
        return Observable.create { subscriber ->
            vkPhotoRemoteDataSource.getPhotos()
                    .onErrorResumeNext(vkPhotoCacheDataSource.getPhotos())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({response: Response ->
                        vkPhotoCacheDataSource.savePhotos(response)
                        subscriber.onNext(response)
                        subscriber.onComplete()
                    },{ error ->
                        subscriber.onError(error)
                    })
        }
    }

    override fun getPhoto(url: String): Observable<Bitmap> {
        return Observable.create { subscriber ->
            vkPhotoCacheDataSource.getPhoto(url)
                    .onErrorResumeNext(vkPhotoRemoteDataSource.getPhoto(url))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ bitmap ->
                        subscriber.onNext(bitmap)
                        subscriber.onComplete()
                        vkPhotoCacheDataSource.savePhoto(url, bitmap)
                    },{ error ->
                        subscriber.onError(error)
                    })
        }
    }

    companion object {

        private var INSTANCE: VKPhotoRepository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.
         * *
         * @return the [VKPhotoRepository] instance
         */
        @JvmStatic fun getInstance(vkPhotoCacheDataSource: VKPhotoCacheDataSource, vkPhotoRemoteDataSource: VKPhotoDataSource) =
                INSTANCE ?: synchronized(VKPhotoRepository::class.java) {
                    INSTANCE ?: VKPhotoRepository(vkPhotoCacheDataSource, vkPhotoRemoteDataSource)
                            .also { INSTANCE = it }
                }


        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}