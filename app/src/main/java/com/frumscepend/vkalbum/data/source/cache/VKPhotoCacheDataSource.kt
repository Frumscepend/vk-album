package com.frumscepend.vkalbum.data.source.cache

import android.content.Context
import android.graphics.Bitmap
import com.frumscepend.vkalbum.data.Response
import com.frumscepend.vkalbum.data.source.DataNotAvailableException
import com.frumscepend.vkalbum.data.source.VKPhotoDataSource
import com.frumscepend.vkalbum.utils.ImageCacher
import com.frumscepend.vkalbum.utils.TextCacher
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.io.File


class VKPhotoCacheDataSource private constructor(): VKPhotoDataSource {
    override fun getPhotos(): Observable<Response> {
        return Observable.create {
            subscriber ->
            val response = TextCacher().load("all_photos")
            response?.let {
                val resp = Gson().fromJson(it, Response::class.java)
                if (resp.count > 0) {
                    subscriber.onNext(resp)
                    subscriber.onComplete()
                } else {
                    subscriber.onError(DataNotAvailableException())
                }
            } ?: kotlin.run {
                subscriber.onError(DataNotAvailableException())
            }
        }
    }

    override fun getPhoto(url: String): Observable<Bitmap> {
        return Observable.create { subscriber ->
            val bmp = ImageCacher().load(url)
            bmp?.let {
                subscriber.onNext(it)
                subscriber.onComplete()
            } ?: run {
                subscriber.onError(DataNotAvailableException())
            }
        }
    }

    /**
     * Saving list of photos to local storage
     */
    fun savePhotos(response: Response){
        Observable.create<Void> { subscriber ->
            TextCacher().save("all_photos", Gson().toJson(response))
            subscriber.onComplete()
        }.subscribeOn(Schedulers.newThread()).subscribe()
    }

    /**
     * Saving image to local storage
     */
    fun savePhoto(key: String, bitmap: Bitmap) {
        Observable.create<Void> { subscriber ->
            ImageCacher().save(key, bitmap)
            subscriber.onComplete()
        }.subscribeOn(Schedulers.newThread()).subscribe()
    }


    private fun getTempFile(context: Context, name: String): File? =
            File.createTempFile(name, null, context.cacheDir)

    companion object {

        private var INSTANCE: VKPhotoCacheDataSource? = null

        /**
         * Returns the single instance of this class, creating it if necessary.
         * *
         * @return the [VKPhotoCacheDataSource] instance
         */
        @JvmStatic fun getInstance() =
                INSTANCE ?: synchronized(VKPhotoCacheDataSource::class.java) {
                    INSTANCE ?: VKPhotoCacheDataSource()
                            .also { INSTANCE = it }
                }


        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}