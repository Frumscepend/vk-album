package com.frumscepend.vkalbum.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

class Photo {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("album_id")
    @Expose
    var albumId: Int? = null
    @SerializedName("owner_id")
    @Expose
    var ownerId: Int? = null
    @SerializedName("photo_75")
    @Expose
    var photo75: String? = null
    @SerializedName("photo_130")
    @Expose
    var photo130: String? = null
    @SerializedName("photo_604")
    @Expose
    var photo604: String? = null
    @SerializedName("photo_807")
    @Expose
    var photo807: String? = null
    @SerializedName("photo_1280")
    @Expose
    var photo1280: String? = null
    @SerializedName("photo_2560")
    @Expose
    var photo2560: String? = null
    @SerializedName("width")
    @Expose
    var width: Int? = null
    @SerializedName("height")
    @Expose
    var height: Int? = null
    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("date")
    @Expose
    var date: Long? = null
    @SerializedName("post_id")
    @Expose
    var postId: Int? = null

    fun getLargestPhoto(): String?{
        return when {
            photo2560 != null -> photo2560
            photo1280 != null -> photo1280
            photo807 != null -> photo807
            photo604 != null -> photo604
            photo130 != null -> photo130
            photo75 != null -> photo75
            else -> null
        }
    }

    fun getStringDate(): String {
        return SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss").format(Date(date!!*1000))
    }
}