package com.frumscepend.testapp

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import com.frumscepend.vkalbum.Injection
import com.frumscepend.vkalbum.data.source.VKPhotoDataSource
import com.frumscepend.vkalbum.main.viewmodel.MainViewModel
import com.frumscepend.vkalbum.main.viewmodel.PhotoDetailsViewModel
import com.frumscepend.vkalbum.main.viewmodel.PhotoViewModel

/**
 * A creator is used to inject the product ID into the ViewModel
 */
class ViewModelFactory private constructor(
        private val application: Application,
        private val vkPhotoDataSource: VKPhotoDataSource
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(MainViewModel::class.java) ->
                        MainViewModel(application, vkPhotoDataSource)
                    isAssignableFrom(PhotoDetailsViewModel::class.java) ->
                        PhotoDetailsViewModel(application, vkPhotoDataSource)
                    isAssignableFrom(PhotoViewModel::class.java) ->
                        PhotoViewModel(application, vkPhotoDataSource)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(application,
                            Injection.provideVKPhotoRepository())
                            .also { INSTANCE = it }
                }


        @VisibleForTesting fun destroyInstance() {
            INSTANCE = null
        }
    }
}
