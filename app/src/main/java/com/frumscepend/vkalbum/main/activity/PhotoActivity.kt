package com.frumscepend.vkalbum.main.activity

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.frumscepend.vkalbum.R
import com.frumscepend.vkalbum.databinding.ActivityPhotoBinding
import com.frumscepend.vkalbum.main.adapter.PhotosAdapter
import com.frumscepend.vkalbum.main.viewmodel.PhotoViewModel
import com.frumscepend.vkalbum.utils.obtainViewModel

class PhotoActivity : AppCompatActivity() {

    private lateinit var activityPhotoBinding: ActivityPhotoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityPhotoBinding = DataBindingUtil.setContentView(this, R.layout.activity_photo)
        activityPhotoBinding.viewmodel = obtainViewModel(PhotoViewModel::class.java)
        activityPhotoBinding.viewmodel?.let { viewmodel ->
            viewmodel.position.set(intent.getIntExtra("position", 0))
            viewmodel.photosAdapter = PhotosAdapter(viewModel = viewmodel, fm = supportFragmentManager)
        }
    }

    override fun onResume() {
        super.onResume()
        activityPhotoBinding.viewmodel?.loadPictures()
    }
}
