package com.frumscepend.vkalbum.main.activity

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.AdapterView
import com.frumscepend.vkalbum.R
import com.frumscepend.vkalbum.databinding.ActivityMainBinding
import com.frumscepend.vkalbum.main.viewmodel.MainViewModel
import com.frumscepend.vkalbum.utils.obtainViewModel
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError


@Suppress("UNUSED_ANONYMOUS_PARAMETER")
class MainActivity : AppCompatActivity() {

    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        activityMainBinding.viewmodel = obtainViewModel(MainViewModel::class.java)
        activityMainBinding.viewmodel?.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val intent = Intent(this@MainActivity, PhotoActivity::class.java)
            intent.putExtra("position", position)
            startActivity(intent)
        }
        if (VKSdk.isLoggedIn()) {
            activityMainBinding.viewmodel?.loadPictures()
        } else {
            VKSdk.login(this, "photos")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
                    override fun onResult(res: VKAccessToken) {
                        activityMainBinding.viewmodel?.loadPictures()
                    }
                    override fun onError(error: VKError) {

                    }
                })) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
