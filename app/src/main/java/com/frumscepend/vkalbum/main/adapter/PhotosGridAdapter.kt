package com.frumscepend.vkalbum.main.adapter

import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.frumscepend.vkalbum.data.Photo
import com.frumscepend.vkalbum.data.source.VKPhotoDataSource
import com.frumscepend.vkalbum.databinding.ItemGridImageBinding


class PhotosGridAdapter constructor(
        private val vkPhotoRepository: VKPhotoDataSource
) : BaseAdapter() {
    var photos: ObservableList<Photo> = ObservableArrayList()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: PhotosViewHolder
        if (convertView == null) {
            val itemBinding = ItemGridImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            holder = PhotosViewHolder(itemBinding)
            holder.view.tag = holder
        } else {
            holder = convertView.tag as PhotosViewHolder
        }
        photos[position].photo130?.let { url ->
            vkPhotoRepository.getPhoto(url)
                    .subscribe { bitmap ->
                        holder.bind(bitmap)
                    }
        }
        return holder.view
    }

    override fun getItem(position: Int): Any {
        return photos[position]
    }

    override fun getItemId(position: Int): Long {
        return photos[position].id?.toLong()!!
    }

    override fun getCount(): Int {
        return photos.size
    }

    private class PhotosViewHolder internal constructor(private val binding: ItemGridImageBinding) {
        val view: View = binding.root

        fun bind(bitmap: Bitmap){
            binding.image = bitmap
        }

    }
}