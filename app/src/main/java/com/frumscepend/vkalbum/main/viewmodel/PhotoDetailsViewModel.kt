package com.frumscepend.vkalbum.main.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.graphics.Bitmap
import com.frumscepend.vkalbum.data.Photo
import com.frumscepend.vkalbum.data.source.VKPhotoDataSource

class PhotoDetailsViewModel(
        context: Application,
        private val vkPhotoRepository: VKPhotoDataSource
): AndroidViewModel(context) {
    var photo = Photo()
    val fail = ObservableBoolean(false)
    val image: ObservableField<Bitmap> = ObservableField()

    /**
     * Loading requested picture
     */
    fun loadPhoto(){
        photo.getLargestPhoto()?.let { url ->
            vkPhotoRepository.getPhoto(url)
                    .subscribe({ bitmap ->
                        fail.set(false)
                        image.set(bitmap)
                    },{
                        fail.set(true)
                    })
        }
    }

}