package com.frumscepend.vkalbum.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.BufferedInputStream


class ImageLoader {

    companion object {

        fun LoadBitmapFromUrl(url: String): Bitmap? {
            val client = OkHttpClient()
            val request = Request.Builder().url(url).build()
            return try {
                val inputStream = client.newCall(request).execute()?.body()?.byteStream()
                val bufferedInputStream = BufferedInputStream(inputStream)
                BitmapFactory.decodeStream(bufferedInputStream)
            } catch (e: Exception) {
                null
            }
        }
    }
}