package com.frumscepend.vkalbum.utils

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.vk.sdk.VKUIHelper.getApplicationContext
import java.io.*


class ImageCacher {

    fun save(key: String, bitmapImage: Bitmap): String {
        val cw = ContextWrapper(getApplicationContext())
        // path to /data/data/app/app_data/imageDir
        val directory = cw.getDir("imageDir", Context.MODE_PRIVATE)
        // Create imageDir
        val mypath = File(directory, key.hashCode().toString())

        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath)
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return directory.absolutePath
    }

    fun load(key: String): Bitmap? {
        try {
            val cw = ContextWrapper(getApplicationContext())
            val directory = cw.getDir("imageDir", Context.MODE_PRIVATE)
            val f = File(directory, key.hashCode().toString())
            return BitmapFactory.decodeStream(FileInputStream(f))
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return null
    }
}