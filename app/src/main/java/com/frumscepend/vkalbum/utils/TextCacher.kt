package com.frumscepend.vkalbum.utils

import android.content.Context
import android.content.ContextWrapper
import com.vk.sdk.VKUIHelper.getApplicationContext
import java.io.*


class TextCacher {

    fun save(key: String, text: String): String {
        val cw = ContextWrapper(getApplicationContext())
        // path to /data/data/app/app_data/imageDir
        val directory = cw.getDir("cacheDir", Context.MODE_PRIVATE)
        // Create imageDir
        val mypath = File(directory, key.hashCode().toString())

        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath)
            fos.write(text.toByteArray())
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return directory.absolutePath
    }

    fun load(key: String): String? {
        try {
            val cw = ContextWrapper(getApplicationContext())
            val directory = cw.getDir("cacheDir", Context.MODE_PRIVATE)
            val file = File(directory, key.hashCode().toString())
            val bReader = BufferedReader(FileReader(file))
            val stringBuilder = StringBuilder()
            val ls = System.getProperty("line.separator")
            bReader.use { reader ->
                var line = reader.readLine()
                while (line != null) {
                    stringBuilder.append(line)
                    stringBuilder.append(ls)
                    line = reader.readLine()
                }
                return stringBuilder.toString()
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return null
    }
}